let event = [];

const initialEvent = sessionStorage.getItem('eventState');
const element = document.getElementById('list');

function getEventString(el) {
    const dateEvent = new Date(`${el.date} ${el.time}`);
    const remaining = new Date(dateEvent - new Date());
    const months = remaining.getMonth();
    const seconds = remaining.getSeconds();
    const days = remaining.getDay();
    const hours = remaining.getHours();
    const minutes = remaining.getMinutes();

    return `Название события: ${el.name}, 
        Осталось: ${months} мес. ${days} д. ${hours} ч. ${minutes} мин. ${seconds} сек.`
}


if (initialEvent) {
    event = JSON.parse(initialEvent);
    event.map(el => {
        const newEvent = document.createElement('li');
        newEvent.id = el.id;
        newEvent.innerHTML = getEventString(el);
        return element.append(newEvent)
    })
}

function addElementInDOM() {
    const newEvent = document.createElement('li');
    const el = event[event.length - 1];
    newEvent.id = el.id;
    newEvent.innerHTML = getEventString(el);
    return element.append(newEvent)
}

function addDataInState() {
    const nameEvent = document.getElementById('name-event').value;
    const dateEvent = document.getElementById('date-event').value;
    const timeEvent = document.getElementById('time-event').value;
    if (nameEvent === '' || dateEvent === '') {
        alert('Название и дата события обязательны')
    } else if (new Date(`${dateEvent} ${timeEvent}`) < new Date()) {
        alert('Введенное время уже состоялось')
    } else {
        event.push(
            {
                name: nameEvent,
                date: dateEvent,
                time: timeEvent === '' ? '00:00:00' : timeEvent,
                id: Math.random().toString(16).slice(-8)
            });
        document.getElementById('name-event').value = '';
        document.getElementById('date-event').value = '';
        document.getElementById('time-event').value = '';
        sessionStorage.setItem('eventState', JSON.stringify(event));
        addElementInDOM()
    }
}

function updateTime() {
    event.map(el => {
        const event = document.getElementById(`${el.id}`);
        event.innerHTML = getEventString(el)
    })
}

setInterval(() => {
    if (event.length > 0)
        updateTime()
}, 1000);


